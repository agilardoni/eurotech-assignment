// Generated from /home/gilaquila/Projects/git/gitlab.com/agilardoni/eurotech-assignment/src/main/antlr4/com/eurotech/assignment/filter/Filter.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FilterParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		OPERAND=1, OR=2, AND=3, EQ=4, NEQ=5, GT=6, LT=7, GTE=8, LTE=9, REGMATCH=10, 
		NOT=11, QUOTES=12, OPENPAREN=13, CLOSEPAREN=14;
	public static final int
		RULE_comparison = 0, RULE_predicate = 1;
	private static String[] makeRuleNames() {
		return new String[] {
			"comparison", "predicate"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, "'||'", "'&&'", "'=='", "'!='", "'>'", "'<'", "'>='", "'<='", 
			"'~='", "'!'", "'\"'", "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "OPERAND", "OR", "AND", "EQ", "NEQ", "GT", "LT", "GTE", "LTE", 
			"REGMATCH", "NOT", "QUOTES", "OPENPAREN", "CLOSEPAREN"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Filter.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public FilterParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ComparisonContext extends ParserRuleContext {
		public Token op;
		public List<TerminalNode> QUOTES() { return getTokens(FilterParser.QUOTES); }
		public TerminalNode QUOTES(int i) {
			return getToken(FilterParser.QUOTES, i);
		}
		public List<TerminalNode> OPERAND() { return getTokens(FilterParser.OPERAND); }
		public TerminalNode OPERAND(int i) {
			return getToken(FilterParser.OPERAND, i);
		}
		public TerminalNode LTE() { return getToken(FilterParser.LTE, 0); }
		public TerminalNode GTE() { return getToken(FilterParser.GTE, 0); }
		public TerminalNode LT() { return getToken(FilterParser.LT, 0); }
		public TerminalNode GT() { return getToken(FilterParser.GT, 0); }
		public TerminalNode REGMATCH() { return getToken(FilterParser.REGMATCH, 0); }
		public TerminalNode EQ() { return getToken(FilterParser.EQ, 0); }
		public TerminalNode NEQ() { return getToken(FilterParser.NEQ, 0); }
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
	}

	public final ComparisonContext comparison() throws RecognitionException {
		ComparisonContext _localctx = new ComparisonContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_comparison);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4);
			match(QUOTES);
			setState(5);
			match(OPERAND);
			setState(6);
			match(QUOTES);
			setState(7);
			((ComparisonContext)_localctx).op = _input.LT(1);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NEQ) | (1L << GT) | (1L << LT) | (1L << GTE) | (1L << LTE) | (1L << REGMATCH))) != 0)) ) {
				((ComparisonContext)_localctx).op = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(8);
			match(QUOTES);
			setState(9);
			match(OPERAND);
			setState(10);
			match(QUOTES);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateContext extends ParserRuleContext {
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
	 
		public PredicateContext() { }
		public void copyFrom(PredicateContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class UnaryExprContext extends PredicateContext {
		public Token op;
		public TerminalNode OPENPAREN() { return getToken(FilterParser.OPENPAREN, 0); }
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public TerminalNode CLOSEPAREN() { return getToken(FilterParser.CLOSEPAREN, 0); }
		public TerminalNode NOT() { return getToken(FilterParser.NOT, 0); }
		public UnaryExprContext(PredicateContext ctx) { copyFrom(ctx); }
	}
	public static class BinaryExprContext extends PredicateContext {
		public Token op;
		public List<TerminalNode> OPENPAREN() { return getTokens(FilterParser.OPENPAREN); }
		public TerminalNode OPENPAREN(int i) {
			return getToken(FilterParser.OPENPAREN, i);
		}
		public List<PredicateContext> predicate() {
			return getRuleContexts(PredicateContext.class);
		}
		public PredicateContext predicate(int i) {
			return getRuleContext(PredicateContext.class,i);
		}
		public List<TerminalNode> CLOSEPAREN() { return getTokens(FilterParser.CLOSEPAREN); }
		public TerminalNode CLOSEPAREN(int i) {
			return getToken(FilterParser.CLOSEPAREN, i);
		}
		public TerminalNode AND() { return getToken(FilterParser.AND, 0); }
		public TerminalNode OR() { return getToken(FilterParser.OR, 0); }
		public BinaryExprContext(PredicateContext ctx) { copyFrom(ctx); }
	}
	public static class ComparisonExprContext extends PredicateContext {
		public ComparisonContext comparison() {
			return getRuleContext(ComparisonContext.class,0);
		}
		public ComparisonExprContext(PredicateContext ctx) { copyFrom(ctx); }
	}
	public static class EncapsulatedExprContext extends PredicateContext {
		public TerminalNode OPENPAREN() { return getToken(FilterParser.OPENPAREN, 0); }
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public TerminalNode CLOSEPAREN() { return getToken(FilterParser.CLOSEPAREN, 0); }
		public EncapsulatedExprContext(PredicateContext ctx) { copyFrom(ctx); }
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_predicate);
		int _la;
		try {
			setState(30);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				_localctx = new UnaryExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(12);
				((UnaryExprContext)_localctx).op = match(NOT);
				setState(13);
				match(OPENPAREN);
				setState(14);
				predicate();
				setState(15);
				match(CLOSEPAREN);
				}
				break;
			case 2:
				_localctx = new BinaryExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(17);
				match(OPENPAREN);
				setState(18);
				predicate();
				setState(19);
				match(CLOSEPAREN);
				setState(20);
				((BinaryExprContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==OR || _la==AND) ) {
					((BinaryExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(21);
				match(OPENPAREN);
				setState(22);
				predicate();
				setState(23);
				match(CLOSEPAREN);
				}
				break;
			case 3:
				_localctx = new EncapsulatedExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(25);
				match(OPENPAREN);
				setState(26);
				predicate();
				setState(27);
				match(CLOSEPAREN);
				}
				break;
			case 4:
				_localctx = new ComparisonExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(29);
				comparison();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20#\4\2\t\2\4\3\t"+
		"\3\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3!\n\3\3\3\2\2\4\2\4\2\4\3\2\6"+
		"\f\3\2\4\5\2#\2\6\3\2\2\2\4 \3\2\2\2\6\7\7\16\2\2\7\b\7\3\2\2\b\t\7\16"+
		"\2\2\t\n\t\2\2\2\n\13\7\16\2\2\13\f\7\3\2\2\f\r\7\16\2\2\r\3\3\2\2\2\16"+
		"\17\7\r\2\2\17\20\7\17\2\2\20\21\5\4\3\2\21\22\7\20\2\2\22!\3\2\2\2\23"+
		"\24\7\17\2\2\24\25\5\4\3\2\25\26\7\20\2\2\26\27\t\3\2\2\27\30\7\17\2\2"+
		"\30\31\5\4\3\2\31\32\7\20\2\2\32!\3\2\2\2\33\34\7\17\2\2\34\35\5\4\3\2"+
		"\35\36\7\20\2\2\36!\3\2\2\2\37!\5\2\2\2 \16\3\2\2\2 \23\3\2\2\2 \33\3"+
		"\2\2\2 \37\3\2\2\2!\5\3\2\2\2\3 ";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}