/** 
 * File definition for filter parsing grammar:
 * the grammar is defined as a composition of predicates. 
 * A predicate is a boolean expression which can either be:
 * 1- comparison between a content of a key and a value:
 *      + "key" comparisonOp "Value"
 *      + comparisonOp can be: [==, >, <, <=, >=, ~=(regex matching)]
 *      + value is represented as a string, in case of [>, <, <=, >=] operators it is casted to int/float
 * 2- boolean expression  between Predicates defined as follow:
 *       + Predicate booleanOperator Predicate
 *       + unaryBooleanOperator Predicate (Maximum priority)
 *       + booleanOperator can be [&&, ||]
 *       + unaryBooleanOperator can be [!]
 */

grammar Filter;

comparison:
	QUOTES OPERAND QUOTES op = (
		LTE
		| GTE
		| LT
		| GT
		| REGMATCH
		| EQ
		| NEQ
	) QUOTES OPERAND QUOTES;

predicate:
	op = NOT OPENPAREN predicate CLOSEPAREN # unaryExpr
	| OPENPAREN predicate CLOSEPAREN op = (
		AND <assoc = right>
		| OR <assoc = right>
	) OPENPAREN predicate CLOSEPAREN	# binaryExpr
	| OPENPAREN predicate CLOSEPAREN	# encapsulatedExpr
	| comparison						# comparisonExpr;

OPERAND: [0-9A-Za-z._ ]+;
OR: '||';
AND: '&&';
EQ: '==';
NEQ: '!=';
GT: '>';
LT: '<';
GTE: '>=';
LTE: '<=';
REGMATCH: '~=';
NOT: '!';

QUOTES: '"';
OPENPAREN: '(';
CLOSEPAREN: ')';
