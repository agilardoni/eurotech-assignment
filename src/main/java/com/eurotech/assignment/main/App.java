package com.eurotech.assignment.main;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.eurotech.assignment.filter.Filter;
import com.eurotech.assignment.filter.StringFilter;
import com.eurotech.assignment.resource.Resource;

public class App {

    public static void main(String[] args) throws Exception {

        // Single resource definition
        Resource user = new Resource(new LinkedHashMap<>());
        user.put("firstname", "Joe");
        user.put("surname", "Bloggs");
        user.put("role", "administrator");
        user.put("age", "35");

        // The filter can be used to match a single resource
        Filter f = new Filter(resource -> resource.containsKey("age") && Integer.parseInt(resource.get("age")) > 30);
        System.out.printf("filter match result: %b\n", f.matches(user));

        f = new Filter(resource -> resource.containsKey("age") && Integer.parseInt(resource.get("age")) < 30);
        System.out.printf("filter match result: %b\n", f.matches(user));

        f = new Filter(resource -> resource.containsKey("age") && Integer.parseInt(resource.get("age")) >= 30);
        System.out.printf("filter match result: %b\n", f.matches(user));

        // Filter that test the resoruce key access to be case insensitive
        f = new Filter(resource -> resource.containsKey("AgE") && Integer.parseInt(resource.get("AgE")) >= 30);
        System.out.printf("filter match result: %b\n", f.matches(user));

        // List of resources definition
        List<Resource> users = new ArrayList<>();
        users.add(user);

        // mockup of some data
        Resource u = new Resource(new LinkedHashMap<>());
        u.put("firstname", "Gino");
        u.put("surname", "Rossi");
        u.put("role", "employee");
        u.put("age", "25");
        users.add(u);

        u = new Resource(new LinkedHashMap<>());
        u.put("firstname", "Alberto");
        u.put("surname", "Rossi");
        u.put("role", "CEO");
        u.put("age", "66");
        users.add(u);

        u = new Resource(new LinkedHashMap<>());
        u.put("firstname", "Sara");
        u.put("surname", "Rossi");
        u.put("role", "office worker");
        u.put("age", "32");
        users.add(u);

        // A filter can be used to obtain a subset of a collection matching a specific
        // criteria
        f = new Filter(resource -> resource.containsKey("age") && resource.containsKey("role")
                && Integer.parseInt(resource.get("age")) > 30 && resource.get("role").equals("administrator"));
        var res = f.filter(users).collect(Collectors.toList());

        System.out.printf("filter match result: %d\n", res.size());

        // Testing StringFilter
        StringFilter sf;

        sf = new StringFilter("(\"age\">\"35\")");
        System.out.printf("StringFilter match result: %b\n", sf.matches(user));
        sf.matches(user);

        sf = new StringFilter("(\"age\"<\"35\")");
        System.out.printf("StringFilter match result: %b\n", sf.matches(user));
        sf.matches(user);

        sf = new StringFilter("(\"age\"<\"35\")&&(\"role\"==\"administrator\")");
        System.out.printf("StringFilter match result: %b\n", sf.matches(user));
        sf.matches(user);
    }
}