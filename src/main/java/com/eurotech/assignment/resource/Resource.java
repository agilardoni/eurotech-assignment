package com.eurotech.assignment.resource;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * This class has the purpose of encapsulating the resource map representation
 * and enforce the required case sensitivity. In order to make the key case
 * insensitive it is enforced to be lower case
 */
public class Resource {
    private Map<String, String> properties;

    public Resource(Map<String, String> map) {
        properties = map;
    }

    public int size() {
        return properties.size();
    }

    public boolean isEmpty() {
        return properties.isEmpty();
    }

    public boolean containsKey(String key) { // FIXME: put constraints on key value
        return properties.containsKey(key.toLowerCase());
    }

    public boolean containsValue(String value) {
        return properties.containsValue(value);
    }

    public String get(String key) {
        return properties.get(key.toLowerCase());
    }

    public String put(String key, String value) {
        return properties.put(key.toLowerCase(), value);
    }

    public String remove(String key) {
        return properties.remove(key.toLowerCase());
    }

    // public void putAll(Map<? extends String, ? extends String> map) {
    // for (var k : map.keySet()) {

    // }

    // properties.putAll(map);
    // }

    public void clear() {
        properties.clear();
    }

    public Set<String> keySet() {
        return properties.keySet();
    }

    public Collection<String> values() {
        return properties.values();
    }

    public Set<java.util.Map.Entry<String, String>> entrySet() {
        return properties.entrySet();
    }
}
