package com.eurotech.assignment.filter;

import com.eurotech.assignment.filter.FilterParser.PredicateContext;
import com.eurotech.assignment.filter.structure.BinaryPredicate;
import com.eurotech.assignment.filter.structure.ComparisonPredicate;
import com.eurotech.assignment.filter.structure.Predicate;
import com.eurotech.assignment.filter.structure.UnaryPredicate;
import com.eurotech.assignment.filter.structure.UnaryPredicate.UnaryOperator;
import com.eurotech.assignment.resource.Resource;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class StringFilter extends Filter {
    private Predicate rootPredicate;

    /**
     * Create a filter from the string representation
     * A predicate is a boolean expression which can either be:
     * 1- comparison between a content of a key and a value:
     *      + "key" comparisonOp "Value"
     *      + comparisonOp can be: [==, >, <, <=, >=, ~=(regex matching)]
     *      + value is represented as a string, in case of [>, <, <=, >=] operators it is casted to int/float
     * 2- boolean expression  between Predicates defined as follow:
     *       + Predicate booleanOperator Predicate
     *       + unaryBooleanOperator Predicate (Maximum priority)
     *       + booleanOperator can be [&&, ||]
     *       + unaryBooleanOperator can be [!]
     * example:
     * ("age" > "30") && ("role" == "administration")
     * 
     * @param filter string representation for the filter
     */
    public StringFilter(String filter) {
        super();

        rootPredicate = new UnaryPredicate(null, UnaryOperator.NONE);

        FilterLexer l = new FilterLexer(CharStreams.fromString(filter));
        CommonTokenStream commonTokenStream = new CommonTokenStream(l);
        FilterParser parser = new FilterParser(commonTokenStream);

        PredicateContext parseTree = parser.predicate();
        ParseTreeWalker walker = new ParseTreeWalker();

        walker.walk(new FilterListener(), parseTree);

        super.setPredicate(resource -> this.evaluate(resource));
    }

    private class FilterListener extends FilterBaseListener {
        private Predicate cursor = rootPredicate;

        @Override
        public void enterComparison(FilterParser.ComparisonContext ctx) {
            // System.out.printf("enterComparison %s, operator %s \n", ctx.OPERAND(),
            // ctx.op.getType());

            var operand = ctx.OPERAND();
            ComparisonPredicate.ComparisonOperator operator = null;
            switch (ctx.op.getType()) {
            case FilterParser.GT:
                operator = ComparisonPredicate.ComparisonOperator.GT;
                break;
            case FilterParser.LT:
                operator = ComparisonPredicate.ComparisonOperator.LT;
                break;
            case FilterParser.LTE:
                operator = ComparisonPredicate.ComparisonOperator.LTE;
                break;
            case FilterParser.EQ:
                operator = ComparisonPredicate.ComparisonOperator.EQ;
                break;
            case FilterParser.NEQ:
                operator = ComparisonPredicate.ComparisonOperator.NEQ;
                break;
            case FilterParser.REGMATCH:
                operator = ComparisonPredicate.ComparisonOperator.NEQ;
                break;
            default:
                throw new RuntimeException(
                        "unknown operator: " + FilterParser.VOCABULARY.getSymbolicName(ctx.op.getType()));
            }

            ComparisonPredicate cmppr = new ComparisonPredicate(cursor, operand.get(0).getText(), operator,
                    operand.get(1).getText());

            setChildren(cursor, cmppr);
        }

        @Override
        public void enterEncapsulatedExpr(FilterParser.EncapsulatedExprContext ctx) {
            // System.out.printf("enterEncapsulatedExpr %s\n", ctx.getText());

            Predicate tmp = new UnaryPredicate(cursor, UnaryOperator.NONE);

            setChildren(cursor, tmp);
            cursor = tmp;

        }

        @Override
        public void enterBinaryExpr(FilterParser.BinaryExprContext ctx) {
            // System.out.printf("enterBinaryExpr %s, %s\n", ctx.getText(),
            // ctx.op.getType());

            BinaryPredicate.BooleanOperator operator = null;
            switch (ctx.op.getType()) {
            case FilterParser.AND:
                operator = BinaryPredicate.BooleanOperator.AND;
                break;
            case FilterParser.OR:
                operator = BinaryPredicate.BooleanOperator.OR;
                break;
            }

            BinaryPredicate tmp = new BinaryPredicate(cursor);
            tmp.setOperator(operator);

            setChildren(cursor, tmp);
            cursor = tmp;

        }

        @Override
        public void enterUnaryExpr(FilterParser.UnaryExprContext ctx) {
            // System.out.printf("enterUnaryExpr %s\n", ctx.getText());

            UnaryPredicate.UnaryOperator operator = null;
            switch (ctx.op.getType()) {
            case FilterParser.NOT:
                operator = UnaryPredicate.UnaryOperator.NOT;
                break;
            }

            UnaryPredicate tmp = new UnaryPredicate(cursor, operator);

            setChildren(cursor, tmp);
            cursor = tmp;

        }

        @Override
        public void exitBinaryExpr(FilterParser.BinaryExprContext ctx) {
            // System.out.printf("exitBinaryExpr %s\n", ctx.getText());
            if (cursor.getFather() != null) {
                cursor = cursor.getFather();
            }
        }

        @Override
        public void exitUnaryExpr(FilterParser.UnaryExprContext ctx) {
            // System.out.printf("exitUnaryExpr %s\n", ctx.getText());
            if (cursor.getFather() != null) {
                cursor = cursor.getFather();
            }
        }

        @Override
        public void exitEncapsulatedExpr(FilterParser.EncapsulatedExprContext ctx) {
            // System.out.printf("exitEncapsulatedExpr %s\n", ctx.getText());
            if (cursor.getFather() != null) {
                cursor = cursor.getFather();
            }
        }

        @Override
        public void visitErrorNode(ErrorNode node) {
            // System.out.printf("visitErrorNode %s\n", node.getText());

            throw new RuntimeException("unable to parse predicate");
        }

        private void setChildren(Predicate father, Predicate child) {
            if (father instanceof UnaryPredicate) {
                UnaryPredicate f = (UnaryPredicate) father;
                f.setChild(child);
            } else if (father instanceof BinaryPredicate) {
                BinaryPredicate f = (BinaryPredicate) father;

                /*
                 * NOTE: Assuming the following two constraints 1- binary operators are
                 * commutative 2- the parser goes first in the left most term
                 */

                if (f.getLeft() == null) {
                    f.setLeft(child);
                } else if (f.getRight() == null) {
                    f.setRight(child);
                } else {
                    // NOTE: this should never happen
                    throw new RuntimeException("father binary predicate has the two child already occupied");
                }
            } else if (father instanceof ComparisonPredicate) {

            } else {
                throw new RuntimeException("unkwnown predicate type " + father.getClass().getName());
            }
        }
    }

    protected void displayTree() {

        displaySubTree(rootPredicate, 0);
    }

    private void displaySubTree(Predicate node, int level) {
        if (node instanceof UnaryPredicate) {
            UnaryPredicate n = (UnaryPredicate) node;

            printPadding(level);
            System.out.println(n.getOperator());

            displaySubTree(n.getChild(), level + 1);
        } else if (node instanceof BinaryPredicate) {
            BinaryPredicate n = (BinaryPredicate) node;

            printPadding(level);
            System.out.println(n.getOperator());

            displaySubTree(n.getLeft(), level + 1);
            displaySubTree(n.getRight(), level + 1);

        } else if (node instanceof ComparisonPredicate) {
            ComparisonPredicate n = (ComparisonPredicate) node;

            printPadding(level);
            // System.out.printf("%s %s %s\n", n.getKey(), n.getOperator(), n.getValue());
        }
        // else if (node instanceof Predicate) {

        // }
    }

    private void printPadding(int padding) {
        for (int i = 0; i < padding; i++) {
            System.out.print(" ");
        }
    }

    private boolean evaluate(Resource resource) {
        return iterateSubTree(rootPredicate, resource);
    }

    private boolean iterateSubTree(Predicate node, Resource resource) {
        boolean res = true;

        if (node instanceof UnaryPredicate) {
            UnaryPredicate n = (UnaryPredicate) node;

            res = !iterateSubTree(n.getChild(), resource);
        } else if (node instanceof BinaryPredicate) {
            BinaryPredicate n = (BinaryPredicate) node;

            boolean res1 = iterateSubTree(n.getLeft(), resource);
            boolean res2 = iterateSubTree(n.getRight(), resource);

            switch (n.getOperator()) {
            case OR:
                res = res1 || res2;
                break;
            case AND:
                res = res1 && res2;
                break;
            }

        } else if (node instanceof ComparisonPredicate) {
            ComparisonPredicate n = (ComparisonPredicate) node;

            res = resource.containsKey(n.getKey());

            if (res) {
                String value = resource.get(n.getKey());

                float fv1, fv2;

                switch (n.getOperator()) {
                case EQ:
                    res = value.equals(n.getValue());
                    break;
                case NEQ:
                    res = !value.equals(n.getValue());
                    break;
                case GT:
                    fv1 = Integer.parseInt(value);
                    fv2 = Integer.parseInt(n.getValue());
                    res = fv1 > fv2;
                    break;
                case LT:
                    fv1 = Integer.parseInt(value);
                    fv2 = Integer.parseInt(n.getValue());
                    res = fv1 < fv2;
                    break;
                case GTE:
                    fv1 = Integer.parseInt(value);
                    fv2 = Integer.parseInt(n.getValue());
                    res = fv1 >= fv2;
                    break;
                case LTE:
                    fv1 = Integer.parseInt(value);
                    fv2 = Integer.parseInt(n.getValue());
                    res = fv1 <= fv2;
                    break;
                case REG_MATCH:
                    throw new UnsupportedOperationException("Regex matching not implemented yet");
                // break;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String predicateExample;
        // predicateExample = "\"a\"==\"1123123\"";
        // predicateExample = "((((\"a\"==\"1123123\"))))";
        // predicateExample =
        // "(((\"a\"==\"1123123\")&&(\"a\"==\"1123123\"))||((\"a\"==\"1123123\")&&(\"a\"==\"1123123\")))";
        // predicateExample = "((\"a\"==\"1123123\")||(\"a\"==\"1123123\"))";
        predicateExample = "((\"a\"==\"12345\")||(\"a\"==\"54321\"))&&(!(\"a\"==\"1123123\"))";
        // predicateExample = "!(\"a\"==\"1123123\")";

        // predicateExample = "not(true)";
        // predicateExample = "(true)and(false)";
        // predicateExample = "(())";

        // System.out.println(predicateExample);

        StringFilter filter = new StringFilter(predicateExample);

        filter.displayTree();

    }
}
