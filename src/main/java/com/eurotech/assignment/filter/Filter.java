package com.eurotech.assignment.filter;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.eurotech.assignment.resource.Resource;

public class Filter {
    private Predicate<Resource> predicate;

    public Filter(Predicate<Resource> predicate) {
        this.predicate = predicate;
    }

    public Filter() {
        this.predicate = null;
    }

    public boolean matches(Resource resource) {
        if (predicate == null) {
            throw new NullPointerException("Predicate undefined");
        }

        return predicate.test(resource);
    }

    public Stream<Resource> filter(List<Resource> resources) {
        if (predicate == null) {
            throw new NullPointerException("Predicate undefined");
        }

        return resources.stream().filter(predicate);
    }

    public Predicate<Resource> getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate<Resource> predicate) {
        this.predicate = predicate;
    }
}