package com.eurotech.assignment.filter.structure;

public class BinaryPredicate extends Predicate {
    private Predicate left;
    private Predicate right;
    private BooleanOperator operator;

    public BinaryPredicate(Predicate father) {
        super(father);
    }

    public BinaryPredicate(Predicate father, Predicate left, BooleanOperator operator, Predicate right) {
        super(father);
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    public Predicate getLeft() {
        return this.left;
    }

    public Predicate getRight() {
        return this.right;
    }

    public BooleanOperator getOperator() {
        return this.operator;
    }

    public void setLeft(Predicate left) {
        this.left = left;
    }

    public void setRight(Predicate right) {
        this.right = right;
    }

    public void setOperator(BooleanOperator operator) {
        this.operator = operator;
    }

    public enum BooleanOperator {
        AND("and"), OR("or");

        public final String representation;

        private BooleanOperator(String representation) {
            this.representation = representation;
        }
    }
}
