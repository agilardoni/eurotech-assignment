package com.eurotech.assignment.filter.structure;

public class ComparisonPredicate extends Predicate {
    private String key;
    private String value;
    private ComparisonOperator operator;

    public ComparisonPredicate(Predicate father) {
        super(father);
    }

    public ComparisonPredicate(Predicate father, String key, ComparisonOperator operator, String value) {
        super(father);
        this.key = key;
        this.value = value;
        this.operator = operator;
    }

    public String getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }

    public ComparisonOperator getOperator() {
        return this.operator;
    }

    public enum ComparisonOperator {
        EQ("=="), NEQ("!="), GT(">"), GTE(">="), LT(">"), LTE(">="), REG_MATCH("~=");

        public final String representation;

        private ComparisonOperator(String representation) {
            this.representation = representation;
        }
    }
}
