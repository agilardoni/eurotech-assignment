package com.eurotech.assignment.filter.structure;

public class UnaryPredicate extends Predicate {
    private UnaryOperator operator;
    private Predicate child;

    public Predicate getChild() {
        return this.child;
    }

    public void setChild(Predicate child) {
        this.child = child;
    }

    public UnaryPredicate() {
    }

    public UnaryPredicate(Predicate father) {
        super(father);
    }

    public UnaryPredicate(Predicate father, UnaryOperator operator) {
        super(father);
        this.operator = operator;
    }

    public UnaryOperator getOperator() {
        return this.operator;
    }

    public void setOperator(UnaryOperator operator) {
        this.operator = operator;
    }

    public enum UnaryOperator {
        NOT("not"), NONE("None");

        public final String representation;

        private UnaryOperator(String representation) {
            this.representation = representation;
        }
    }
}
