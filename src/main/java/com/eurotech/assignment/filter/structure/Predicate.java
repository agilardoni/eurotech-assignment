package com.eurotech.assignment.filter.structure;

public class Predicate {
    private Predicate father;

    public Predicate() {
    }

    public Predicate(Predicate father) {
        this.father = father;
    }

    public Predicate getFather() {
        return this.father;
    }

    public void setFather(Predicate father) {
        this.father = father;
    }

    public Predicate father(Predicate father) {
        setFather(father);
        return this;
    }
}
