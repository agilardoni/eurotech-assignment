# Filter 

This application is intended to implement a filtering api for resource objects. 
A resource is a caollection of key value association, basically Java `Map<String, String>`, in which keys are case insensitive. 
Resources are implemented in the class `com.eurotech.assignment.resource.Resource`.
A filter is able to detect if a resource matches certain criteria with its properties.

## Filter class
There are 2 class of filters currently developed: `Filter` and `StringFilters`.
- `Filter` takes a `java.util.function.Predicate<Resource>` as parameter and this is used to extract a boolean expression
- `StringFilter` takes a String representation of a filter and using a parser builds a boolean evaluating function, the syntax is briefly explained in the comments

